package com.test.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.test.entity.AddressE;
import com.test.entity.IdentificationE;
import com.test.model.Identification;
import com.test.repository.AddressRepo;
import com.test.repository.IdentificationRepo;

@Controller
@RequestMapping("/contacts/")
public class ContactController {
	
	private final IdentificationRepo identRepo;
	private final AddressRepo addRepo;
	
	@Autowired
	public ContactController(IdentificationRepo identRepo, AddressRepo addRepo) {
		this.identRepo = identRepo;
		this.addRepo = addRepo;
	}
	
	@GetMapping("new")
  public String showAddForm(Model model) {
		
	  model.addAttribute("identity", new IdentificationE());
    return "contactForm";
  }
	
	@GetMapping("{id}/new")
  public String showAddForm(@PathVariable("id") long id, Model model) {
    
	  IdentificationE entity = identRepo.findById(id)
        .orElseThrow(() -> new IllegalArgumentException("Invalid Indentification Id:" + id));
	  
	  AddressE address = new AddressE();
	  address.setIdentity(entity);
    model.addAttribute("address", address);
    return "addressForm";
  }
	
	@GetMapping("list")
	public String listContacts(Model model){						
		
		model.addAttribute("identifications", identRepo.findAll());
		
		return "home";
	}
	
	@PostMapping("add/{id}")
    public String addContact(@PathVariable("id") long id, @Valid IdentificationE identity, BindingResult result, Model model,RedirectAttributes redirectAttributes) {
		
        if (result.hasErrors()) {
        	System.out.println("Error......"+result.toString());
        	model.addAttribute("identity", identity);
            return "contactForm";
        }
        identity.setId(id);        
        this.identRepo.save(identity);
        
        redirectAttributes.addFlashAttribute("message", "Successful!");
        
        return "redirect:/contacts/list";
    }
	
	@GetMapping("edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
		    IdentificationE entity = identRepo.findById(id)
            .orElseThrow(() -> new IllegalArgumentException("Invalid Indentification Id:" + id));
		    System.out.println("Size of the Address..."+entity.getAddressSet().size());
        model.addAttribute("identity", entity);
        return "contactForm";
    }
	
	@GetMapping("delete/{id}")
    public String deleteIdentification(@PathVariable("id") long id, Model model, RedirectAttributes redirectAttributes) {
		    IdentificationE entity = identRepo.findById(id)
            .orElseThrow(() -> new IllegalArgumentException("Invalid Indentification Id:" + id));
		    identRepo.delete(entity);
        model.addAttribute("identity", identRepo.findAll());
		
        redirectAttributes.addFlashAttribute("message", "Successful!");
        return "redirect:/contacts/list";
    }
	
	@PostMapping("{identityId}/add/{id}")
	  public String addAddress(@PathVariable("identityId") long identityId, @PathVariable("id") long id, @Valid AddressE address, BindingResult result, Model model,RedirectAttributes redirectAttributes) {
  
      if (result.hasErrors()) {
        System.out.println("Error......"+result.toString());
        model.addAttribute("address", address);
          return "addressForm";
      }
      
      IdentificationE identity = identRepo.findById(identityId)
          .orElseThrow(() -> new IllegalArgumentException("Invalid Indentification Id:" + identityId));
      
      address.setIdentity(identity);
            
      this.addRepo.save(address);
      
      identity = identRepo.findById(identityId)
          .orElseThrow(() -> new IllegalArgumentException("Invalid Indentification Id:" + identityId));
      
      redirectAttributes.addFlashAttribute("message", "Successful!");
      model.addAttribute("identity", identity);
      return "redirect:/contacts/edit/"+identityId;
    }
}
