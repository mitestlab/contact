package com.test.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.test.entity.IdentificationE;

@Repository
public interface IdentificationRepo extends CrudRepository<IdentificationE, Long> {

}
