/**
 * 
 */
package com.test.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.entity.AddressE;

/**
 * @author SUBRAMANIAN-BHARATHI
 *
 */
public interface AddressRepo  extends CrudRepository<AddressE, Long> {

}
