package com.test.model;

import com.test.enums.CommunicationTypeEnum;

public class Communication {
	
	private long id;
	private CommunicationTypeEnum communicationType;
	private String value;
	private boolean preferred;
		
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public CommunicationTypeEnum getCommunicationType() {
		return communicationType;
	}
	public void setCommunicationType(CommunicationTypeEnum communicationType) {
		this.communicationType = communicationType;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public boolean isPreferred() {
		return preferred;
	}
	public void setPreferred(boolean preferred) {
		this.preferred = preferred;
	}
	
	
	
}
