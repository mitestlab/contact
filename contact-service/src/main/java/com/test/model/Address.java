package com.test.model;

import com.test.enums.AddressTypeEnum;

public class Address {
	
	private long id;
	private AddressTypeEnum addressType;
	private int streetNumber;
	private String streetName;
	private String unit;
	private String city;
	private String state;
	private int zipCode;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public AddressTypeEnum getAddressType() {
		return addressType;
	}
	public void setAddressType(AddressTypeEnum addressType) {
		this.addressType = addressType;
	}
	public int getStreetNumber() {
		return streetNumber;
	}
	public void setStreetNumber(int streetNumber) {
		this.streetNumber = streetNumber;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getZipCode() {
		return zipCode;
	}
	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}
	
	
	

}
