package com.test.enums;

public enum AddressTypeEnum {
	HOME("home"),
    WORK("work"),   
    UNKNOWN("unknown");

    private String type;

    AddressTypeEnum(String type) {
        this.type = type;
    }

    public String type() {
        return type;
    }
}
