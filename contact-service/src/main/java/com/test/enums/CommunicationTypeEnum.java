package com.test.enums;

public enum CommunicationTypeEnum {
	EMAIL("email"),
    CELL("cell"),   
    UNKNOWN("unknown");

    private String type;

    CommunicationTypeEnum(String type) {
        this.type = type;
    }

    public String type() {
        return type;
    }
}
