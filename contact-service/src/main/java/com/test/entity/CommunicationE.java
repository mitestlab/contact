package com.test.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CommunicationE {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "communicationType")
	private String communicationType;
	
	@Column(name = "value")
	private String value;
	
	@Column(name = "preferred")
	private boolean preferred;

	public CommunicationE() {
		super();		
	}

	public CommunicationE(long id, String communicationType, String value, boolean preferred) {
		super();
		this.id = id;
		this.communicationType = communicationType;
		this.value = value;
		this.preferred = preferred;
	}
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCommunicationType() {
		return communicationType;
	}

	public void setCommunicationType(String communicationType) {
		this.communicationType = communicationType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isPreferred() {
		return preferred;
	}

	public void setPreferred(boolean preferred) {
		this.preferred = preferred;
	}
	
	
	
	
	
}
