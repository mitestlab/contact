package com.test.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="address")
public class AddressE {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "addressType")
	private String addressType;
	
	@Column(name = "streetNumber")
	private int streetNumber;
	
	@Column(name = "streetName")
	private String streetName;
	
	@Column(name = "unit")
	private String unit;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "zipCode")
	private int zipCode;

	@ManyToOne
  @JoinColumn(name = "identity_id")
  private IdentificationE identity;

	
	public AddressE() {
		super();		
	}


	public AddressE(long id, String addressType, int streetNumber, String streetName, String unit, String city,
			String state, int zipCode) {
		super();
		this.id = id;
		this.addressType = addressType;
		this.streetNumber = streetNumber;
		this.streetName = streetName;
		this.unit = unit;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getAddressType() {
		return addressType;
	}


	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}


	public int getStreetNumber() {
		return streetNumber;
	}


	public void setStreetNumber(int streetNumber) {
		this.streetNumber = streetNumber;
	}


	public String getStreetName() {
		return streetName;
	}


	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}


	public String getUnit() {
		return unit;
	}


	public void setUnit(String unit) {
		this.unit = unit;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public int getZipCode() {
		return zipCode;
	}


	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}


  /**
   * @return the identity
   */
  public IdentificationE getIdentity() {
    return identity;
  }


  /**
   * @param identity the identity to set
   */
  public void setIdentity(IdentificationE identity) {
    this.identity = identity;
  }
	
	
	
}
